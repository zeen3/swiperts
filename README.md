# Swiperts

some shit that makes swipes emit events on media.

runs async rather than sync. tries to use newer tech wherever possible.

## Usage

You can import it with ESNext modules, AMD or a custom thing.

```js
import {Swiper} from './swiper.mjs'
```

```js
const Swiper = require('./swiper.js')
```

When done; use ``Swiper.init`` or ``new Swiper()`` to initalise as so:

```js
let swi = Swiper.init(swiperTarget, {
	threshold: 100,
	restraint: 50,
	restrain: false,
	time: Infinity, // measured in milliseconds; compared using `DOMHighResTimeStamp`s. The maximum amount of time before the event is cancelled.
	velocity: 1.0, // minimum velocity of the fling in px/ms. Adjust as needed. Setting to 0 will likely cause issues.
	noMouse: false, // allows fling through mousedown and mouseup events.
	noKB: false // allows fling when the element is focused and the user pressed the Arrow(Left|Right|Up|Down) keys. Alternatively can be a HTMLElement such as document.body in case it's preffered that it receives events from there.
})
swi.surface.addEventListener('swipe', e => {
	switch (e.detail.dir) {
		case 'e': break;
		case 'w': break;
	}
})
```

A short form is availabe as `new Swiper`:

```js
let swi = new Swiper(swiperTarget, threshold, restraint, restrain, time, velocity, noMouse, noKB)
// ...
```

The element itself is given a "swipe" event dispatch; which can be handled either through the swipers' `surface` property or through the DOM.

The event dispatch is done through an internal `nextTick`/`setImmediate` filler which allows the swiper to run entirely asyncrounously given a supporting environment.

It can be used in a `type=module` script if you use the `mjs` extension versions; or through `requirejs`'s `require()`. I'm working on a way to make it autoadd if there's no normal moduling available so... hang in there?

