
export interface SwiperOptions {
	threshold?: number;
	restraint?: number;
	restrain?: boolean;
	time?: number;
	velocity?: number;
	noMouse?: boolean;
	noKB?: boolean;
}
export interface SwipeEvents {
	'': SwiperEvent;
	'n': SwiperEvent;
	'ne': SwiperEvent;
	'e': SwiperEvent;
	'se': SwiperEvent;
	's': SwiperEvent;
	'sw': SwiperEvent;
	'w': SwiperEvent;
	'nw': SwiperEvent;
}
export interface SwiperEventInit {
	dir: keyof SwipeEvents;
	dx: number;
	dy: number;
	dt: number;
	_x: number;
	_y: number;
	sx: number;
	sy: number;
	st: number;
	ex: number;
	ey: number;
	et: number;
	init: number;
	last: number;
	velocity: number;
}
export type SwiperEvent = CustomEventInit<SwiperEventInit>;

export interface SwiperNextTik {
	(fn: (hrt?: DOMHighResTimeStamp) => void): void;
};

// export interface HTMLSwipableElement extends HTMLElement {
// 	addEventListener(type: "swipe", listener: (this: HTMLElement, ev: SwiperEvent) => any, options?: boolean | AddEventListenerOptions): void;
// 	removeEventListener(type: "swipe", listener: (this: HTMLElement, ev: SwiperEvent) => any, options?: boolean | EventListenerOptions): void;
// }

export class Swiper {
	static readonly getPerf = 'performance' in window && 'function' === typeof performance.now
		? () => performance.now()
		: () => Date.now()
	private static readonly nextTik:SwiperNextTik = ((()=>{
		try {
			if ('setImmediate' in window && 'performance' in window && 'function' === typeof performance.now)
				return (fn: (hrt?: DOMHighResTimeStamp) => void) => {window.setImmediate(fn, performance.now())}
			// messagechan is async - it's intended for workers but meh
			if ('MessageChannel' in window) {
				const {port1, port2} = new MessageChannel,
					q:((hrt?: DOMHighResTimeStamp) => void)[] = []
				port2.onmessage = (e: MessageEvent) => {
					let c = q.shift()
					if (c) c.call(e.data, performance.now())
				}
				const nextTick:SwiperNextTik = (fn: (hrt?: DOMHighResTimeStamp) => void) => {
					if ('function' !== typeof fn)
						throw new TypeError('bad nexttik type')
					q.push(fn)
					port1.postMessage(null)
				}
				return nextTick
			}
		} finally {
			const msg = 'next-tick|' + Math.random().toString(32).substring(2),
				q:((hrt?: DOMHighResTimeStamp) => void)[] = [],
				msgh = (e: MessageEvent) => {
					if (e.data === msg) {
						let c = q.shift()
						if (c) c.call(null, Swiper.getPerf())
						e.stopPropagation()
					}
				},
				nextTick:SwiperNextTik = (fn: (hrt?: DOMHighResTimeStamp) => void) => {
					if ('function' !== typeof fn)
						throw new TypeError('bad nexttik type')
					q.push(fn)
					window.postMessage(msg, '*')
				}
			window.addEventListener('message', msgh)
			return nextTick
		}
	})())
	private vl:Float64Array;
	private listenerOpts: EventListenerOptions;
	private cleared:boolean;

	private _onRestrain: (evt: MouseEvent | TouchEvent) => void;
	private _onStart: (evt: MouseEvent | TouchEvent) => void;
	private _onEnd: (evt: MouseEvent | TouchEvent) => void;
	// private _onkeyUp: (evt: KeyboardEvent) => void;
	private _onKeyDown: (evt: KeyboardEvent) => void;

	static getDir(_x:number, _y:number):keyof SwipeEvents {
		switch ((_x+1) << 2 | (_y+1)) {
			// sorted for ease of reading
			case 6: return 'n'
			case 10:return 'ne'
			case 9: return 'e'
			case 8: return 'se'
			case 4: return 's'
			case 0: return 'sw'
			case 1: return 'w'
			case 2: return 'nw'

			// default empty (none or 0,0)
			default:return ''
		}
	}

	static getPageXY(evt: MouseEvent | TouchEvent):MouseEvent | Touch {
		return evt instanceof MouseEvent ? evt : evt.changedTouches[evt.changedTouches.length - 1]
	}

	static init(surface?:HTMLElement, opts:SwiperOptions = {}):Swiper {
		if (!(surface && surface instanceof HTMLElement))
			throw new TypeError('Swiper.init was not called with or on a HTMLElement')
		return new Swiper(
			surface,
			opts.threshold,
			opts.restraint,
			opts.restrain,
			opts.time,
			opts.velocity,
			opts.noMouse,
			opts.noKB
		)
	}

	constructor(
		public surface: HTMLElement /*| HTMLSwipableElement*/,
		// surface to fling off of
		private threshold:number = 100,
		// minimum distance to automatically trigger an event
		private restraint:number = Math.ceil(threshold / 2),
		// maximum distance to automatically trigger an event
		private restrain:boolean = false,
		// restrain scroll within the surface
		private time:number = Infinity,
		// maximum time of fling
		private velocity:number = 1.0,
		// minimum velocity of fling in pixels per millisecond
		private noMouse:boolean = false,
		// disable mouse fling
		private noKB:boolean | HTMLElement = false
		// disable LeftArrow/RightArrow/UpArrow/DownArrow calls
	) {
		if (restraint > threshold)
			throw new RangeError('The restraint must not be greater than the threshold.')
		this.cleared = false

		this.vl = new Float64Array(8)
		this.vl[0] = Swiper.getPerf()
		const opts = {passive: !restrain, capture: true}
		this.listenerOpts = opts

		this._onStart = this.onStart.bind(this)
		this._onEnd = this.onEnd.bind(this)
		this._onRestrain = this.onRestrain.bind(this)
		this._onKeyDown = this.onKeyDown.bind(this)
		// this._onKeyUp = this.onKeyUp.bind(this)

		surface.addEventListener('touchstart', this._onStart, opts)
		surface.addEventListener('touchend', this._onEnd, opts)
		if (restrain) surface.addEventListener('touchmove', this._onRestrain, opts)
		if (!noMouse) {
			surface.addEventListener('mousedown', this._onStart, opts)
			surface.addEventListener('mouseup', this._onEnd, opts)
		}
		if (!noKB) {
			surface.addEventListener('keydown', this._onKeyDown, opts)
			// surface.addEventListener('keyup', this._onKeyUp, opts)
		}
		if (noKB instanceof HTMLElement)
			noKB.addEventListener('keydown', this._onKeyDown, opts)
	}
	clear() {
		this.surface.removeEventListener('touchstart', this._onStart, this.listenerOpts)
		this.surface.removeEventListener('touchend', this._onEnd, this.listenerOpts)
		this.surface.removeEventListener('touchmove', this._onRestrain, this.listenerOpts)
		if (!this.noMouse) {
			this.surface.removeEventListener('mousedown', this._onStart, this.listenerOpts)
			this.surface.removeEventListener('mouseup', this._onEnd, this.listenerOpts)
		}
		if (!this.noKB) this.surface.removeEventListener('keydown', this._onKeyDown, this.listenerOpts)
		delete this._onEnd
		delete this._onStart
		delete this._onRestrain

		// delete this._onKeyUp
		delete this._onKeyDown

		delete this.vl
		delete this.listenerOpts
		delete this.surface

		this.cleared = true
	}
	clrchk():never | void {
		if (this.cleared) throw new ReferenceError('This surface has been cleared.')
	}

	get dx():number {return this.ex - this.sx}
	get dy():number {return this.ey - this.sy}
	get dt():number {return this.et - this.st}

	get sx():number{return this.vl[1]}
	set sx(x:number){this.vl[1] = x}
	get sy():number{return this.vl[2]}
	set sy(y:number){this.vl[2] = y}
	get st():number{return this.vl[3]}
	set st(t:number){this.vl[3] = t}

	get ex():number{return this.vl[4]}
	set ex(x:number){this.vl[4] = x}
	get ey():number{return this.vl[5]}
	set ey(y:number){this.vl[5] = y}
	get et():number{return this.vl[6]}
	set et(t:number){this.vl[6] = t}

	dispatch(_x:number, _y:number, dx:number, dy:number, dt:number, v:number) {
		this.clrchk()
		const data:SwiperEventInit = {
			dir: Swiper.getDir(_x, _y),
			dx: dx,
			dy: dy,
			dt: dt,
			_x: _x,
			_y: _y,
			sx: this.sx,
			sy: this.sy,
			st: this.st,
			ex: this.ex,
			ey: this.ey,
			et: this.et,
			init: this.vl[0],
			last: this.vl[7],
			velocity: v
		}
		const evt = new CustomEvent('swipe', {detail: data})
		const {surface} = this
		Swiper.nextTik(() => surface.dispatchEvent(evt))
		this.vl[7] = Swiper.getPerf()
	}
	onRestrain(evt:MouseEvent | TouchEvent) {
		this.clrchk()
		return evt.preventDefault()
	}
	onStart(evt:MouseEvent | TouchEvent) {
		this.clrchk()
		this.st = evt.timeStamp || Swiper.getPerf()
		this.sx = Swiper.getPageXY(evt).pageX
		this.sy = Swiper.getPageXY(evt).pageY
		if (this.restrain) evt.preventDefault()
	}
	onEnd(evt:MouseEvent | TouchEvent) {
		this.clrchk()
		this.et = evt.timeStamp || Swiper.getPerf()
		this.ex = Swiper.getPageXY(evt).pageX
		this.ey = Swiper.getPageXY(evt).pageY
		if (this.restrain) evt.preventDefault()

		const {dx, dy, dt} = this
		const v = 1e3 * Math.sqrt(dx*dx+dy*dy) / dt

		if (v >= this.velocity || dt <= this.time) {
			const ax = Math.abs(dx),
				ay = Math.abs(dy),
				_x = dx > 0 ? 1 : dx < 0 ? -1 : 0,
				_y = dy > 0 ? 1 : dy < 0 ? -1 : 0

			if (ax >= this.threshold && ay <= this.restraint) switch (_x) {
				case +1: return this.dispatch(+1, 0, dx, dy, dt, v)
				case -1: return this.dispatch(-1, 0, dx, dy, dt, v)
				default: break
			}
			if (ay >= this.threshold && ax <= this.restraint) switch (_y) {
				case +1: return this.dispatch(0, +1, dx, dy, dt, v)
				case -1: return this.dispatch(0, -1, dx, dy, dt, v)
				default: break
			}
			if (ay >= this.threshold && ax >= this.threshold)
				return this.dispatch(_x, _y, dx, dy, dt, v)
		}
	}
	onKeyDown(evt:KeyboardEvent):void {
		if (evt.altKey || evt.ctrlKey || evt.metaKey || evt.shiftKey) return void 0

		switch (evt.key) {
			case 'Left':
			case 'ArrowLeft':
				return this.dispatch(-1, +0, -Infinity, 0, 1, Infinity)
			case 'Right':
			case 'ArrowRight':
				return this.dispatch(+1, +0, +Infinity, 0, 1, Infinity)
			case 'Up':
			case 'ArrowUp':
				return this.dispatch(+0, +1, 0, +Infinity, 1, Infinity)
			case 'Down':
			case 'ArrowDown':
				return this.dispatch(+0, -1, 0, -Infinity, 1, Infinity)
			default: return void 0
		}
	}
}
if (!('InitSwiper' in window)) Object.defineProperty(window, 'InitSwiper', {
	value: Swiper.init,
	enumerable: true,
	configurable: true
})

// making it work with exports
export const init = Swiper.init
export default Swiper
export const name = 'Swiper'
