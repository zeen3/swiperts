export interface SwiperOptions {
    threshold?: number;
    restraint?: number;
    restrain?: boolean;
    time?: number;
    velocity?: number;
    noMouse?: boolean;
    noKB?: boolean;
}
export interface SwipeEvents {
    '': SwiperEvent;
    'n': SwiperEvent;
    'ne': SwiperEvent;
    'e': SwiperEvent;
    'se': SwiperEvent;
    's': SwiperEvent;
    'sw': SwiperEvent;
    'w': SwiperEvent;
    'nw': SwiperEvent;
}
export interface SwiperEventInit {
    dir: keyof SwipeEvents;
    dx: number;
    dy: number;
    dt: number;
    _x: number;
    _y: number;
    sx: number;
    sy: number;
    st: number;
    ex: number;
    ey: number;
    et: number;
    init: number;
    last: number;
    velocity: number;
}
export declare type SwiperEvent = CustomEventInit<SwiperEventInit>;
export interface SwiperNextTik {
    (fn: (hrt?: DOMHighResTimeStamp) => void): void;
}
export declare class Swiper {
    surface: HTMLElement;
    private threshold;
    private restraint;
    private restrain;
    private time;
    private velocity;
    private noMouse;
    private noKB;
    static readonly getPerf: () => number;
    private static readonly nextTik;
    private vl;
    private listenerOpts;
    private cleared;
    private _onRestrain;
    private _onStart;
    private _onEnd;
    private _onKeyDown;
    static getDir(_x: number, _y: number): keyof SwipeEvents;
    static getPageXY(evt: MouseEvent | TouchEvent): MouseEvent | Touch;
    static init(surface?: HTMLElement, opts?: SwiperOptions): Swiper;
    constructor(surface: HTMLElement, threshold?: number, restraint?: number, restrain?: boolean, time?: number, velocity?: number, noMouse?: boolean, noKB?: boolean | HTMLElement);
    clear(): void;
    clrchk(): never | void;
    readonly dx: number;
    readonly dy: number;
    readonly dt: number;
    sx: number;
    sy: number;
    st: number;
    ex: number;
    ey: number;
    et: number;
    dispatch(_x: number, _y: number, dx: number, dy: number, dt: number, v: number): void;
    onRestrain(evt: MouseEvent | TouchEvent): void;
    onStart(evt: MouseEvent | TouchEvent): void;
    onEnd(evt: MouseEvent | TouchEvent): void;
    onKeyDown(evt: KeyboardEvent): void;
}
export declare const init: typeof Swiper.init;
export default Swiper;
export declare const name = "Swiper";
//# sourceMappingURL=swiper.d.ts.map