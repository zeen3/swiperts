define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    ;
    var Swiper = (function () {
        function Swiper(surface, threshold, restraint, restrain, time, velocity, noMouse, noKB) {
            if (threshold === void 0) { threshold = 100; }
            if (restraint === void 0) { restraint = Math.ceil(threshold / 2); }
            if (restrain === void 0) { restrain = false; }
            if (time === void 0) { time = Infinity; }
            if (velocity === void 0) { velocity = 1.0; }
            if (noMouse === void 0) { noMouse = false; }
            if (noKB === void 0) { noKB = false; }
            this.surface = surface;
            this.threshold = threshold;
            this.restraint = restraint;
            this.restrain = restrain;
            this.time = time;
            this.velocity = velocity;
            this.noMouse = noMouse;
            this.noKB = noKB;
            if (restraint > threshold)
                throw new RangeError('The restraint must not be greater than the threshold.');
            this.cleared = false;
            this.vl = new Float64Array(8);
            this.vl[0] = Swiper.getPerf();
            var opts = { passive: !restrain, capture: true };
            this.listenerOpts = opts;
            this._onStart = this.onStart.bind(this);
            this._onEnd = this.onEnd.bind(this);
            this._onRestrain = this.onRestrain.bind(this);
            this._onKeyDown = this.onKeyDown.bind(this);
            surface.addEventListener('touchstart', this._onStart, opts);
            surface.addEventListener('touchend', this._onEnd, opts);
            if (restrain)
                surface.addEventListener('touchmove', this._onRestrain, opts);
            if (!noMouse) {
                surface.addEventListener('mousedown', this._onStart, opts);
                surface.addEventListener('mouseup', this._onEnd, opts);
            }
            if (!noKB) {
                surface.addEventListener('keydown', this._onKeyDown, opts);
            }
            if (noKB instanceof HTMLElement)
                noKB.addEventListener('keydown', this._onKeyDown, opts);
        }
        Swiper.getDir = function (_x, _y) {
            switch ((_x + 1) << 2 | (_y + 1)) {
                case 6: return 'n';
                case 10: return 'ne';
                case 9: return 'e';
                case 8: return 'se';
                case 4: return 's';
                case 0: return 'sw';
                case 1: return 'w';
                case 2: return 'nw';
                default: return '';
            }
        };
        Swiper.getPageXY = function (evt) {
            return evt instanceof MouseEvent ? evt : evt.changedTouches[evt.changedTouches.length - 1];
        };
        Swiper.init = function (surface, opts) {
            if (opts === void 0) { opts = {}; }
            if (!(surface && surface instanceof HTMLElement))
                throw new TypeError('Swiper.init was not called with or on a HTMLElement');
            return new Swiper(surface, opts.threshold, opts.restraint, opts.restrain, opts.time, opts.velocity, opts.noMouse, opts.noKB);
        };
        Swiper.prototype.clear = function () {
            this.surface.removeEventListener('touchstart', this._onStart, this.listenerOpts);
            this.surface.removeEventListener('touchend', this._onEnd, this.listenerOpts);
            this.surface.removeEventListener('touchmove', this._onRestrain, this.listenerOpts);
            if (!this.noMouse) {
                this.surface.removeEventListener('mousedown', this._onStart, this.listenerOpts);
                this.surface.removeEventListener('mouseup', this._onEnd, this.listenerOpts);
            }
            if (!this.noKB)
                this.surface.removeEventListener('keydown', this._onKeyDown, this.listenerOpts);
            delete this._onEnd;
            delete this._onStart;
            delete this._onRestrain;
            delete this._onKeyDown;
            delete this.vl;
            delete this.listenerOpts;
            delete this.surface;
            this.cleared = true;
        };
        Swiper.prototype.clrchk = function () {
            if (this.cleared)
                throw new ReferenceError('This surface has been cleared.');
        };
        Object.defineProperty(Swiper.prototype, "dx", {
            get: function () { return this.ex - this.sx; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Swiper.prototype, "dy", {
            get: function () { return this.ey - this.sy; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Swiper.prototype, "dt", {
            get: function () { return this.et - this.st; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Swiper.prototype, "sx", {
            get: function () { return this.vl[1]; },
            set: function (x) { this.vl[1] = x; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Swiper.prototype, "sy", {
            get: function () { return this.vl[2]; },
            set: function (y) { this.vl[2] = y; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Swiper.prototype, "st", {
            get: function () { return this.vl[3]; },
            set: function (t) { this.vl[3] = t; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Swiper.prototype, "ex", {
            get: function () { return this.vl[4]; },
            set: function (x) { this.vl[4] = x; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Swiper.prototype, "ey", {
            get: function () { return this.vl[5]; },
            set: function (y) { this.vl[5] = y; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Swiper.prototype, "et", {
            get: function () { return this.vl[6]; },
            set: function (t) { this.vl[6] = t; },
            enumerable: true,
            configurable: true
        });
        Swiper.prototype.dispatch = function (_x, _y, dx, dy, dt, v) {
            this.clrchk();
            var data = {
                dir: Swiper.getDir(_x, _y),
                dx: dx,
                dy: dy,
                dt: dt,
                _x: _x,
                _y: _y,
                sx: this.sx,
                sy: this.sy,
                st: this.st,
                ex: this.ex,
                ey: this.ey,
                et: this.et,
                init: this.vl[0],
                last: this.vl[7],
                velocity: v
            };
            var evt = new CustomEvent('swipe', { detail: data });
            var surface = this.surface;
            Swiper.nextTik(function () { return surface.dispatchEvent(evt); });
            this.vl[7] = Swiper.getPerf();
        };
        Swiper.prototype.onRestrain = function (evt) {
            this.clrchk();
            return evt.preventDefault();
        };
        Swiper.prototype.onStart = function (evt) {
            this.clrchk();
            this.st = evt.timeStamp || Swiper.getPerf();
            this.sx = Swiper.getPageXY(evt).pageX;
            this.sy = Swiper.getPageXY(evt).pageY;
            if (this.restrain)
                evt.preventDefault();
        };
        Swiper.prototype.onEnd = function (evt) {
            this.clrchk();
            this.et = evt.timeStamp || Swiper.getPerf();
            this.ex = Swiper.getPageXY(evt).pageX;
            this.ey = Swiper.getPageXY(evt).pageY;
            if (this.restrain)
                evt.preventDefault();
            var _a = this, dx = _a.dx, dy = _a.dy, dt = _a.dt;
            var v = 1e3 * Math.sqrt(dx * dx + dy * dy) / dt;
            if (v >= this.velocity || dt <= this.time) {
                var ax = Math.abs(dx), ay = Math.abs(dy), _x = dx > 0 ? 1 : dx < 0 ? -1 : 0, _y = dy > 0 ? 1 : dy < 0 ? -1 : 0;
                if (ax >= this.threshold && ay <= this.restraint)
                    switch (_x) {
                        case +1: return this.dispatch(+1, 0, dx, dy, dt, v);
                        case -1: return this.dispatch(-1, 0, dx, dy, dt, v);
                        default: break;
                    }
                if (ay >= this.threshold && ax <= this.restraint)
                    switch (_y) {
                        case +1: return this.dispatch(0, +1, dx, dy, dt, v);
                        case -1: return this.dispatch(0, -1, dx, dy, dt, v);
                        default: break;
                    }
                if (ay >= this.threshold && ax >= this.threshold)
                    return this.dispatch(_x, _y, dx, dy, dt, v);
            }
        };
        Swiper.prototype.onKeyDown = function (evt) {
            if (evt.altKey || evt.ctrlKey || evt.metaKey || evt.shiftKey)
                return void 0;
            switch (evt.key) {
                case 'Left':
                case 'ArrowLeft':
                    return this.dispatch(-1, +0, -Infinity, 0, 1, Infinity);
                case 'Right':
                case 'ArrowRight':
                    return this.dispatch(+1, +0, +Infinity, 0, 1, Infinity);
                case 'Up':
                case 'ArrowUp':
                    return this.dispatch(+0, +1, 0, +Infinity, 1, Infinity);
                case 'Down':
                case 'ArrowDown':
                    return this.dispatch(+0, -1, 0, -Infinity, 1, Infinity);
                default: return void 0;
            }
        };
        Swiper.getPerf = 'performance' in window && 'function' === typeof performance.now
            ? function () { return performance.now(); }
            : function () { return Date.now(); };
        Swiper.nextTik = ((function () {
            try {
                if ('setImmediate' in window && 'performance' in window && 'function' === typeof performance.now)
                    return function (fn) { window.setImmediate(fn, performance.now()); };
                if ('MessageChannel' in window) {
                    var _a = new MessageChannel, port1_1 = _a.port1, port2 = _a.port2, q_1 = [];
                    port2.onmessage = function (e) {
                        var c = q_1.shift();
                        if (c)
                            c.call(e.data, performance.now());
                    };
                    var nextTick = function (fn) {
                        if ('function' !== typeof fn)
                            throw new TypeError('bad nexttik type');
                        q_1.push(fn);
                        port1_1.postMessage(null);
                    };
                    return nextTick;
                }
            }
            finally {
                var msg_1 = 'next-tick|' + Math.random().toString(32).substring(2), q_2 = [], msgh = function (e) {
                    if (e.data === msg_1) {
                        var c = q_2.shift();
                        if (c)
                            c.call(null, Swiper.getPerf());
                        e.stopPropagation();
                    }
                }, nextTick = function (fn) {
                    if ('function' !== typeof fn)
                        throw new TypeError('bad nexttik type');
                    q_2.push(fn);
                    window.postMessage(msg_1, '*');
                };
                window.addEventListener('message', msgh);
                return nextTick;
            }
        })());
        return Swiper;
    }());
    exports.Swiper = Swiper;
    if (!('InitSwiper' in window))
        Object.defineProperty(window, 'InitSwiper', {
            value: Swiper.init,
            enumerable: true,
            configurable: true
        });
    exports.init = Swiper.init;
    exports.default = Swiper;
    exports.name = 'Swiper';
});
//# sourceMappingURL=swiper.js.map
if (!('define' in window && 'function' !== typeof window.define)) function define(rq, fn) { // hoist
var exports = {}
fn(function(){throw new Error('No requirables can be called with this implementation')}, exports)
if ('string' === typeof exports.name) window[exports.name] = exports
}
