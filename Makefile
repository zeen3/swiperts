#!/bin/make
DIST_FOLDER=dist

Makefile: esnext amd .mkln

esnext:
	tsc -t ESNext -m ESNext
	for v in $(DIST_FOLDER)/*.js; do \
		sed \
			-e 's/.js.map/.mjs.map/g' \
			-e "s/from '\\(.\/[^']*\\)'/from '\\1.mjs'/g" \
			-e "s/from '\\(..\/[^']*\\)'/from '\\1.mjs'/g" \
			-e "s/import(\(['\"]\)\([^\1\]\)\1)/import(\\1\\2.mjs\\1)/g" \
			-i "$$v" ; \
		mv "$$v" "$$(dirname "$$v")/$$(basename "$$v" .js).mjs" ; done
	for v in $(DIST_FOLDER)/*.js.map; do \
		mv "$$v" "$$(dirname "$$v")/$$(basename "$$v" .js.map).mjs.map" ; done

umd:
	tsc -m umd -t ES2017

amd:
	tsc -m amd -t ES5
	echo -e "\nif (!('define' in window && 'function' !== typeof window.define)) function define(rq, fn) { // hoist\nvar exports = {}\nfn(function(){throw new Error('No requirables can be called with this implementation')}, exports)\nif ('string' === typeof exports.name) window[exports.name] = exports\n}" >> dist/swiper.js

cjs:
	tsc -m commonjs -t ES2017

none:
	tsc -m none 

.mkln:
	for v in $(DIST_FOLDER)/*.js $(DIST_FOLDER)/*.mjs $(DIST_FOLDER)/*js.map; do if [[ -f "$$v" ]]; then ln -sf "$$v" "./$$(basename "$$v")"; fi; done;

clrsm:
	symlinks -rd .

include clrsm
